# Sterling Language Evaulator
A simple ML-esque dialect implemented in Standard ML for USF's COP 4620 
"Programming Languages".

To use it, simply run `make` to fetch a required SML file from the professor's
public website. This contains the datatype for our expressions and basic types
and acts as a specification (with a few examples) for the language. You may then
include `sterling.sml` to use `tc` for expression typechecking and `eval` for
expression evaluation.
