(*
  I pledge my Honor that I have not cheated and will not cheat on this
  assignment.
  - Kenneth Sills
*)

use "ss.sml"

(**
  Used when the evaluation of a STERLING expression cannot be completed due to
  ill or incomplete typing.
**)
exception Stuck

(**
  Type Check a STERLING expression, returning `(SOME typ)` for well-typed
  expressions; Otherwise `NONE` for poorly typed expressions.

  # Helpers
  * `find_in_context` - Given a string some Tuple list of `(string * typ)`,
                        this function attempts to find the type of the variable.
                        If such an associated type is found, returns
                        `(SOME typ)`; otherwise NONE.
  * `tc_recurse`      - The recursive variant of the function, accepting a
                        the expression e as well as the current context for the
                        type checker. This current context is a Tuple list of
                        `(string * type)` binding bound variable names to their
                        associated types.
**)
fun tc (e: expr) : typ option =
  let
    fun len
        (l: 'a list)
          : int =
      case l of
        []          => 0
      | (e :: rest) => 1 + (len rest)

    fun at
        (i: int)
        (l: 'a list)
          : 'a option =
      case l of
        []        => NONE
      | e :: rest => if i = 1 then (SOME e) else (at (i-1) rest)

    fun any
        (l: 'a list)
        (predicate: 'a -> bool)
          : bool =
      case l of
        []          => false
      | (e :: rest) =>
        if (predicate e)
        then true
        else (any rest predicate)

    fun zip
        (l1: 'a list)
        (l2: 'b list)
          : ('a * 'b) list =
      case l1 of
        [] => []
      | (e1 :: rest1) =>
        (case l2 of
          [] => []
        | (e2 :: rest2) => (e1, e2) :: (zip rest1 rest2))

    fun find_in_context
        (s: string)
        (context: (string * typ) list)
          : typ option =
      case context of
        (id, e) :: l => if (s = id) then (SOME e) else (find_in_context s l)
      | [] => NONE

    fun tc_recurse
        (e: expr)
        (context: (string * typ) list)
          : typ option =

      let fun case_typing
              (cases: (typ * (string * expr)) list)
              (context: (string * typ) list)
                : typ option =
            let fun case_typing_recurse
                (cases: (typ * (string * expr)) list)
                (context: (string * typ) list)
                (running: typ)
                  : typ option =
              (case cases of
                [] => (SOME running)
              | ((t, (s, e)) :: rest) =>
                (case tc_recurse e ((s, t) :: context) of
                  SOME (running) => (case_typing_recurse rest context running)
                | _ => NONE
                ))
            in
            case cases of
              [] => NONE
            | ((t, (s, e)) :: rest) =>
                (case tc_recurse e ((s, t) :: context) of
                  SOME (running) => (case_typing_recurse rest context running)
                | NONE => NONE)
            end
      in
      case e of
        IntExpr _ => SOME Int
      | SuccExpr sub_e =>
        if (tc_recurse sub_e context) = (SOME Int)
        then SOME Int
        else NONE
      | PredExpr sub_e =>
        if (tc_recurse sub_e context) = (SOME Int)
        then SOME Int
        else NONE
      | FunExpr (name, p_name, p_type, r_type, body) =>
        let val new_context =
                (case name of
                   SOME s => [(s, Arrow (p_type, r_type))]
                 | NONE => nil) @
                (case p_name of
                   SOME s => [(s, p_type)]
                 | NONE => nil) @
                context
            val body_type = tc_recurse body new_context
        in
        if body_type = SOME r_type
        then SOME (Arrow (p_type, r_type))
        else NONE
        end
      | ApplyExpr (func, arg) =>
        let val fun_type_opt = tc_recurse func context
            val arg_type_opt = tc_recurse arg context
        in
        (case fun_type_opt of
          SOME fun_type =>
            (case fun_type of
              Arrow (in_type, out_type) =>
                (case arg_type_opt of
                  SOME arg_type =>
                    if in_type = arg_type
                    then SOME out_type
                    else NONE
                | NONE => NONE)
            | _ => NONE)
        | NONE => NONE)
        end
      | VarExpr (id) =>
        find_in_context id context
      | BeqExpr (lhs, rhs, e_on_eq, e_on_neq) =>
        if ((tc_recurse lhs context) = SOME Int) andalso
            ((tc_recurse rhs context) = SOME Int)
        then
          let val on_eq_type = tc_recurse e_on_eq context
              val on_neq_type = tc_recurse e_on_neq context
          in
          if (on_eq_type = on_neq_type) andalso (on_eq_type <> NONE)
          then on_eq_type
          else NONE
          end
        else NONE
      | TupleExpr (values) =>
        (case values of
          [] => NONE
        | e :: [] => NONE
        | e1 :: e2 :: [] =>
          let val mt1 = (tc_recurse e1 context)
              val mt2 = (tc_recurse e2 context)
          in
          (case mt1 of SOME t1 =>
            (case mt2 of SOME t2 => SOME (Prod [t1, t2]) | NONE => NONE)
          | NONE => NONE)
          end
        | e :: rest =>
          (case (tc_recurse e context) of
            SOME (t) =>
              let val tr = (tc_recurse (TupleExpr rest) context)
              in
              (case tr of
                SOME (Prod (ts)) =>
                  (SOME (Prod (t :: ts)))
              | _ => NONE)
              end
          | NONE => NONE))
      | LetExpr (identifiers, value, body) =>
        (case (tc_recurse value context) of
          SOME (Prod (types)) =>
            if (len identifiers) = (len types)
            then (tc_recurse body (context @ (zip identifiers types)))
            else NONE
        | _ => NONE)
      | SumExpr (i, value, variants) =>
        if i <= (len variants)
        then
          if (tc_recurse value context) = (at i variants)
          then SOME (Sum variants)
          else NONE
        else NONE
      | CaseExpr (conditional, cases) =>
        (case (tc_recurse conditional context) of
          SOME (Sum (variants)) =>
            if (len variants) = (len cases)
            then (case_typing (zip variants cases) context)
            else NONE
        | _ => NONE)
      end
  in
  tc_recurse e []
  end

(**
  Evaluate a provided STERLING expression, returning the final result of
  execution. If the expression has ill or incomplete typing or otherwise
  execution cannot be carried out, the `Stuck` exception will be thrown.
**)
fun eval(e: expr) : expr =
  let
      fun find_in_context
          (s: string)
          (context: (string * expr) list)
            : expr option =
        case context of
          (id, e) :: l => if (s = id) then (SOME e) else (find_in_context s l)
        | [] => NONE

      fun at
          (i: int)
          (l: 'a list)
            : 'a option =
        case l of
          []        => NONE
        | e :: rest => if i = 1 then (SOME e) else (at (i-1) rest)

      fun zip
          (l1: 'a list)
          (l2: 'b list)
            : ('a * 'b) list =
        case l1 of
          [] => []
        | (e1 :: rest1) =>
          (case l2 of
            [] => []
          | (e2 :: rest2) => (e1, e2) :: (zip rest1 rest2))

      fun eval_recurse (e: expr) (context: (string * expr) list) : expr =
        case e of
          IntExpr (n) => e
        | SuccExpr (sub_e) =>
          (case sub_e of
            IntExpr n => IntExpr(n + 1)
          | _ => raise Stuck)
        | PredExpr (sub_e) =>
          (case sub_e of
            IntExpr n => IntExpr(n - 1)
          | _ => raise Stuck)
        | FunExpr (name, p_name, _, _, body) => e
        | ApplyExpr (func, arg) =>
            (case func of
              FunExpr (func_name_opt, arg_name_opt, _, _, body) =>
                (eval_recurse body
                    ((case func_name_opt of SOME s => [(s, func)] | NONE => nil) @
                    (case arg_name_opt of SOME s => [(s, arg)] | NONE => nil) @
                    context))
            | _ => raise Stuck)
        | VarExpr (id) =>
          (case (find_in_context id context) of
            SOME (e) => (eval_recurse e context)
          | NONE => raise Stuck)
        | BeqExpr (lhs, rhs, e_on_eq, e_on_neq) =>
          let val lhs_result = (eval_recurse lhs context)
              val rhs_result = (eval_recurse rhs context)
          in
          if (lhs_result = rhs_result)
          then (eval_recurse e_on_eq context)
          else (eval_recurse e_on_neq context)
          end
        | TupleExpr (elements) =>
            TupleExpr (map (fn e => (eval_recurse e context)) elements)
        | LetExpr (names, assignment, body) =>
            (eval_recurse body
                          (zip names
                            (case (eval_recurse assignment context) of
                              TupleExpr (elements) => elements
                            | _ => raise Stuck)))
        | SumExpr (i, assignment, variants) =>
            SumExpr (i, (eval_recurse assignment context), variants)
        | CaseExpr (conditional, cases) =>
            (case (eval_recurse conditional context) of
              SumExpr (i, e, ts) =>
                (case (at i cases) of
                  SOME (s, b) => (eval_recurse b ((s, e) :: context))
                | _ => raise Stuck)
            | _ => raise Stuck)
  in
  if (tc e) = NONE
  then raise Stuck
  else (eval_recurse e [])
  end;
