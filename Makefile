ss-url := www.cse.usf.edu/~ligatti/pl-18/as6/ss.sml
target ?= ss.sml

.PHONY: clean
.DEFAULT: $(target)

.DELETE_ON_ERROR: $(target)

$(target):
	curl $(ss-url) > $@
	sha256sum --check $@.sha256

clean:
	rm $(target)
